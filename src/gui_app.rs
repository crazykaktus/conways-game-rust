use glutin_window::GlutinWindow;
use std::time::{Duration, Instant};
use glutin_window::OpenGL;
use graphics::{Context, rectangle};

use graphics::types::Color;
use opengl_graphics::GlGraphics;
use piston::{Events, EventSettings, RenderArgs, Size, UpdateArgs, WindowSettings};
use piston::input::{RenderEvent, UpdateEvent};

use crate::conway::{Board, Cell};

pub struct DrawParams
{
    pub(crate) board: Board,
    pub(crate) live_cell_color: Color,
    pub(crate) dead_cell_color: Color,
    pub(crate) cell_size: f64,
}

pub struct App
{
    pub(crate) window: GlutinWindow,
    pub(crate) gl: GlGraphics, //must be initialized AFTER GlutinWindow
    pub(crate) draw_params: DrawParams,
    pub(crate) last_update: Instant,
    pub(crate) update_speed: Duration,
}




impl App {
    pub fn render(&mut self, args: &RenderArgs)// matrix: &Vec<Vec<bool>>, context: Context, graphics: &mut G2d)
    {
        use graphics::*;

        self.gl.draw(args.viewport(), |context, gl| {
            // Clear the screen
            clear([0.0; 4], gl);

            render_game_of_life(&self.draw_params, context, gl)
        });
    }


    pub fn update(&mut self, _: &UpdateArgs)
    {
        if self.last_update.elapsed() >= self.update_speed
        {
            self.draw_params.board.next_round(true);
            self.last_update = Instant::now();
        }
    }
}


pub fn board_size_based_on_cell_size(length: usize, cell_size: &usize) -> usize
{
    match cell_size {
        0 => unreachable!(),
        _ => length/cell_size,
    }
}

fn render_game_of_life(draw_params: &DrawParams, context: Context, gl: &mut GlGraphics)
{
    draw_params.board.readable.iter().enumerate().for_each(|(row_idx, row)| {
        row.iter().enumerate().for_each(|(col_idx, &ref cell)| {
            let x = col_idx as f64 * draw_params.cell_size;
            let y = row_idx as f64 * draw_params.cell_size;
            let color = match cell {
                Cell::Alive => draw_params.live_cell_color,
                Cell::Dead => draw_params.dead_cell_color,
            };
            // let color = if cell { self.live_cell_color } else { self.dead_cell_color };

            rectangle(color, [x, y, draw_params.cell_size, draw_params.cell_size], context.transform, gl);
        });
    });
}



pub fn game(app: &mut App)
{
    let mut events = Events::new(EventSettings::new());
    while let Some(event) = events.next(&mut app.window)
    {
        if let Some(args) = event.render_args()
        {
            app.render(&args);
        }

        if let Some(args) = event.update_args()
        {
            app.update(&args);
        }
    }
}

pub fn prepare_window(opengl_version: &OpenGL, title: &str, size: &Size) -> GlutinWindow
{
    let result= WindowSettings::new(title, [size.width, size.height])
        .graphics_api(*opengl_version)
        .exit_on_esc(true)
        .build();
    match result {
        Ok(working_window) => {
            println!("Successfully created window with OpenGL version {:?}", opengl_version);
            working_window
        }
        Err(ex) => {
            panic!("Failed to create window with OpenGL version {:?}: {:?}", opengl_version, ex);
        }
    }
}