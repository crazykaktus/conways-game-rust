use crate::conway::{Board, Cell};

#[allow(dead_code)]
pub fn paint_stillness(board: &mut Board, x: usize, y: usize)
{
    if x + 1 >= board.width() || y + 1 >= board.height() {
        println!("It is out of range of the board");
        return
    }
    board.writable[y][x] = Cell::Alive;
    board.writable[y][x+1] = Cell::Alive;
    board.writable[y+1][x] = Cell::Alive;
    board.writable[y+1][x+1] = Cell::Alive;
}

#[allow(dead_code)]
pub fn paint_ship(board: &mut Board, x: usize, y: usize) {
    if x + 38 >= board.width() || y >= board.height() {
        println!("It is out of range of the board");
        return
    }
    board.writable[y][x] = Cell::Alive;
    board.writable[y][x+1] = Cell::Alive;
    board.writable[y][x+2] = Cell::Alive;
    board.writable[y][x+3] = Cell::Alive;
    board.writable[y][x+4] = Cell::Alive;
    board.writable[y][x+5] = Cell::Alive;
    board.writable[y][x+6] = Cell::Alive;
    board.writable[y][x+7] = Cell::Alive;

    board.writable[y][x+9] = Cell::Alive;
    board.writable[y][x+10] = Cell::Alive;
    board.writable[y][x+11] = Cell::Alive;
    board.writable[y][x+12] = Cell::Alive;
    board.writable[y][x+13] = Cell::Alive;

    board.writable[y][x+17] = Cell::Alive;
    board.writable[y][x+18] = Cell::Alive;
    board.writable[y][x+19] = Cell::Alive;

    board.writable[y][x+26] = Cell::Alive;
    board.writable[y][x+27] = Cell::Alive;
    board.writable[y][x+28] = Cell::Alive;
    board.writable[y][x+29] = Cell::Alive;
    board.writable[y][x+30] = Cell::Alive;
    board.writable[y][x+31] = Cell::Alive;
    board.writable[y][x+32] = Cell::Alive;

    board.writable[y][x+34] = Cell::Alive;
    board.writable[y][x+35] = Cell::Alive;
    board.writable[y][x+36] = Cell::Alive;
    board.writable[y][x+37] = Cell::Alive;
    board.writable[y][x+38] = Cell::Alive;
}