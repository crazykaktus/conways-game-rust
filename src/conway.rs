use std::collections::HashSet;
use std::mem::swap;
use rayon::iter::ParallelIterator;
use rayon::prelude::IntoParallelRefMutIterator;
use rayon::iter::IndexedParallelIterator;

#[derive(Debug)]
#[derive(Clone)]
#[derive(PartialEq)]
pub enum Cell
{
    Alive,
    Dead,
}

//     Any live cell with fewer than two live neighbours dies, as if by underpopulation.
//     Any live cell with two or three live neighbours lives on to the next generation.
//     Any live cell with more than three live neighbours dies, as if by overpopulation.
//     Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
fn decide_life(count: usize, cell: Cell) -> Cell
{
    match cell {
        Cell::Alive => {
            match count {
                0 | 1 => Cell::Dead,
                4 | 5 | 6 | 7 | 8 => Cell::Dead,
                2 | 3 => Cell::Alive,
                _ => unreachable!(),
            }
        },
        Cell::Dead => {
            match count {
                3 => Cell::Alive,
                0 | 1 | 2 | 4 | 5 | 6 | 7 | 8 => Cell::Dead,
                _ => unreachable!(),
            }
        },
    }
}

fn is_alive(cell: &Cell, count: &mut usize)
{
    if cell == &Cell::Alive
    {
        *count += 1;
    }
}

fn neighbour_count_for_elegant(matrix: &[Vec<Cell>], col: usize, row: usize) -> usize
{
    let rows_len = matrix.len();
    let cols_len = matrix.first().map_or(0, Vec::len);

    let mut count = 0;
    for row_index in row.saturating_sub(1)..=row + 1
    {
        if row_index >= rows_len { continue; }

        for col_index in col.saturating_sub(1)..=col + 1
        {
            if col_index >= cols_len || (row_index == row && col_index == col) { continue; }

            is_alive(&matrix[row_index][col_index], &mut count);
        }
    }

    count
}

fn neighbour_count_for(matrix: &Vec<Vec<Cell>>, col: usize, row: usize) -> usize
{
    let mut count: usize = 0;
    if row > 0
    {
        if col + 1 < matrix.first().unwrap().len()
        {
            is_alive(&matrix[row - 1][col + 1], &mut count);
        }

        is_alive(&matrix[row - 1][col], &mut count);

        if col > 0
        {
            is_alive(&matrix[row - 1][col - 1], &mut count);
        }
    }

    if row + 1 < matrix.len()
    {
        if col + 1 < matrix.first().unwrap().len()
        {
            is_alive(&matrix[row + 1][col + 1], &mut count);
        }

        is_alive(&matrix[row + 1][col], &mut count);

        if col > 0
        {
            is_alive(&matrix[row + 1][col - 1], &mut count);
        }
    }


    if col + 1 < matrix.first().unwrap().len()
    {
        is_alive(&matrix[row][col + 1], &mut count);
    }

    if col > 0
    {
        is_alive(&matrix[row][col - 1], &mut count);
    }

    return count;
}


// Instead of rewriting entire matrix, we have only hash of used Alive Cells.
// recommended by ChatGPT - not tested yet
// TODO
#[allow(dead_code)]
fn update_active_cells_elegant(active_cells: &HashSet<(usize, usize)>) -> HashSet<(usize, usize)>
{
    let mut next_active_cells = HashSet::new();

    // A map to count neighbors
    let mut neighbor_counts = std::collections::HashMap::new();

    for &(row, col) in active_cells
    {
        // Increment neighbor count for all neighbors of a live cell
        for r in row.saturating_sub(1)..=row + 1
        {
            for c in col.saturating_sub(1)..=col + 1
            {
                if (r, c) != (row, col)
                {
                    *neighbor_counts.entry((r, c)).or_insert(0) += 1;
                }
            }
        }
    }

    for (&cell, &count) in &neighbor_counts
    {
        if count == 3 || (count == 2 && active_cells.contains(&cell))
        {
            next_active_cells.insert(cell);
        }
    }

    next_active_cells
}


#[derive(Debug)]
pub struct Board
{
    pub(crate) readable: Vec<Vec<Cell>>,
    pub(crate) writable: Vec<Vec<Cell>>,
    width: usize,
    height: usize,
}

impl Board
{
    pub(crate) fn next_round(&mut self, optimized: bool)
    {
        swap(&mut self.readable, &mut self.writable);
        self.writable.par_iter_mut().enumerate().for_each(
            |(row_index, chunk)|
                {
                    for col_index in 0..chunk.len()
                    {
                        let neighbours = if optimized { neighbour_count_for_elegant(&self.readable, col_index, row_index) } else { neighbour_count_for(&self.readable, col_index, row_index) };
                        chunk[col_index] = decide_life(neighbours, self.readable[row_index][col_index].clone());
                    }
                });
    }

    pub fn new(width: usize, height: usize, init_cell: Cell) -> Self
    {
        let row = vec![init_cell; width];
        let board = vec![row; height];
        Self {
            readable: board.clone(),
            writable: board,
            width,
            height,
        }
    }
    pub fn width(&self) -> usize
    {
        self.width
    }
    pub fn height(&self) -> usize
    {
        self.height
    }
}

impl Board
{
    pub fn print_pretty(&self, with_borders: bool)
    {
        let alive = if with_borders { "[*]" } else { " * " };
        let dead = if with_borders { "[ ]" } else { "   " };

        self.writable.iter().for_each(
            |row|
                {
                    row.iter().for_each(
                        |cell|
                            {
                                match cell
                                {
                                    Cell::Alive => print!("{}", alive),
                                    Cell::Dead => print!("{}", dead),
                                }
                            });
                    print!("\n")
                });
        println!();
    }
}