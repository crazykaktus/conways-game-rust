mod conway;
mod example_conway_objects;
mod gui_app;

use crate::gui_app::{App, DrawParams, board_size_based_on_cell_size, game, prepare_window};
use crate::example_conway_objects::paint_ship;
use std::time::{Duration, Instant};
use crate::conway::{Board, Cell};
use graphics::types::Color;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::Size;

fn main()
{
    let screen_size = Size{
        width: 1920.0,// * 2.0,
        height: 1080.0,// * 2.0,
    };

    let white : Color = [1.0, 1.0, 1.0, 1.0];
    let black : Color = [0.0, 0.0, 0.0, 1.0];

    let cell_size: usize = 5;

    let mut board: Board = Board::new(
        board_size_based_on_cell_size(screen_size.width as usize, &cell_size),
        board_size_based_on_cell_size(screen_size.height as usize, &cell_size),
        Cell::Dead
    );

    //init some cells for showcase
    paint_ship(&mut board, (screen_size.width as usize/cell_size) / 10, (screen_size.height as usize/cell_size)/2);

    let title = "Conway's game of life";

    let opengl_version : OpenGL = OpenGL::V4_5; //change this in case of crashes - it is "by design"

    let draw_params = DrawParams{
        board,
        live_cell_color: white,
        dead_cell_color: black,
        cell_size: cell_size as f64,
    };
    let mut app = App {
        window: prepare_window(&opengl_version, title, &screen_size),
        gl: GlGraphics::new(opengl_version),
        draw_params,
        last_update: Instant::now(),
        update_speed: Duration::from_millis(100),
    };
    game(&mut app);
}


//code showcase for non gui usage
#[allow(dead_code)]
fn console_main()
{
    let mut board: Board = Board::new(50, 50, Cell::Dead);

    // println!("{:?}", board);

    board.writable[24][25] = Cell::Alive;
    board.writable[25][24] = Cell::Alive;
    board.writable[24][24] = Cell::Alive;
    board.writable[25][25] = Cell::Alive;

    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
    board.next_round(false);
    board.print_pretty(true);
}



